<?php
use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('users', function ($table) {
    $table->increments('id');
    $table->string('email')->unique();
    $table->timestamps();
});

Capsule::schema()->create('posts', function ($table) {
    $table->increments('id');
    $table->string('title');
    $table->string('description');
    $table->integer('user_id')->unsigned();
	$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
    $table->timestamps();
});

?>

