<?php
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule= new Capsule;

$capsule->addConnection([
	"driver" 	=> "mysql",
	"host"	  	=> "127.0.0.1",
	"username"  => "root",
	"password"  => "",
	"database"  => "blogs",
	"charset"   => "utf8",
	"collation" => "utf8_general_ci",
	"prefix"   	=> "",
	"timezone"  => "+02:00",
	'options'    => [PDO::MYSQL_ATTR_LOCAL_INFILE=>true]

]);
//$capsule->setAsGlobal();
$capsule->bootEloquent();

?>