<?php
 /**
  * base url function 
  * @return [type] [description]
  */
 function base_url(){
  return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    '/mvc'
  );
}

/**
 * [asset description]
 * @return [type] [description]
 */
function asset($uri=''){
	return base_url().'/public/'.ltrim($uri, '/');
}
/**
 * files,images uploaded path
 * @param  string $path [description]
 * @return [type]       [description]
 */
function public_path($path=''){
  return getcwd()."/public/".ltrim($path,'/');

}
/**
 * Dump the passed variables and hault the script. 
 * @param  [mixed] $var [description]
 * @return [type]      [description]
 */
function debug($var){
  echo "<pre>";
  var_dump($var);
  die;

}
/**
 * dynamicaly page script will be loaded from here 
 * @param  string $script script name
 * @return [type]         [description]
 */
function page_script($script){

}
/**
 * error display method
 */
function errorShow($error=false){
  if ($error) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
  }else{
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
  }
}


?>