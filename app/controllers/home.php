<?php

use App\Models\TestModel;
use App\Core\Controller;

/**
 * 
 */
class Home extends Controller
{
	
	public function index(){
		$posts=TestModel::all();
		return $this->view('home/home',["title" => "Home",'posts' =>$posts]);
	}
	public function test(){
		$posts=TestModel::all();
		return $this->view('home/home',["title" => "Test",'posts' =>$posts]);
	}
}


?>