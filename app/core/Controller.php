<?php
namespace App\Core;
Class Controller
{
	/**
	 * dynamically load the model and return model object
	 * @param  string $model [model name]
	 * @return object
	 */
	public function model($model){
		require_once('app/models/'.$model.'.php');
		return new $model;
	}
	/**
	 * dynamically load the view 
	 * @param  string $view [view file name]
	 * @param  mixed $data [data to be passed in view]
	 * @return file      [view file return]
	 */
	public function view($view,$data=[]){
		require_once('app/views/layouts/master.php');

	}
}



?>